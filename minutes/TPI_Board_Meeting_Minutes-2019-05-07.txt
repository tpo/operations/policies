﻿Tor Project Board Meeting Minutes for May 7, 2019.

Phone/video meeting called to order at 16:00 UTC  

Present: Isabela Bagueros, Shari Steele, Matt Blaze, Ramy Raoof,  Biella Coleman, Julius Mittenzwei, Nick Mathewson, Roger Dingledine.
 
1. Finances

– Isabela went over fund raising strategy
– Budget and pay update

Matt joined at 13:00 EDT 

2. Tor Meeting Update

– Update about Tor Meeting in Sweden
 
3. New Tor Board Member update

– Biella provided update about potential new member. 


Meeting Adjourned at 17:35 UTC 



 

 





 
