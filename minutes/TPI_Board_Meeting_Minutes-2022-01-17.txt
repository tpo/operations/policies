Tor Board Meeting Minutes for January 17, 2022

Video and phone conference meeting called to order at, 12:21 pm est 5:23 pm utc

Present: Desigan Chinniah, Biella Coleman,  Kendra Albert Rabbi Rob, Ramy Raoof, Julius Mittenzwei, Matt Blaze (after 12:33)


I.                Board Administration Updates and Motions

-       Motion: The Board moves to approve a three year term for Rabbi Rob,
effective January 17, 2022. Kendra moved the motion, Ramy seconded, and all voted in favor.
-       Motion: Approval of Sept 13 2021 minutes: Kendra moved the motion, Rabbi Rob seconded, all voted in favor.
-       Motion: The Tor Board moves to accept Ramy Raoff’s and Chelsea Kolmo’s resignation, effective January 17, 2022. Kendra moved the motion, Biella seconded, and all voted in favor.
-       Rabbi Rob will write up a description of the Vice-Chair position (roles, responsibilities, expectation and time commitments) and send it to the board for consideration. We will move to identify a person during our next


Meeting adjourned at 12:43
